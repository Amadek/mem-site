$(function() {
    function activate(obj, target, color) {
        obj.removeClass('btn-outline-secondary')
        obj.addClass(color)
        obj.attr('data-target', target)
    }

    function deactivate(obj, color) {
        obj.removeClass(color)
        obj.addClass('btn-outline-secondary')
        obj.attr('data-target', '')
    }

    var user = $('.user')
    var the_name
    user.click(function() {
        // Meme clicked or not.
        if (!$(this).hasClass('table-warning')) {
            $('tr').removeClass('table-warning')
            $(this).addClass('table-warning')
            the_name = $(this).attr('id')
        } else {
            $(this).removeClass('table-warning')
            the_name = null;
        }
        $('#old_name').val(the_name);
        $('.the_name').val(the_name);
        // Trash script
        if (!the_name) {
            deactivate($('#trash'), 'btn-outline-danger')
            deactivate($('#change'), 'btn-outline-warning')
            deactivate($('#pass'), 'btn-outline-warning')
        } else {
           activate($('#trash'), '.removeAcc', 'btn-outline-danger')
           activate($('#change'), '.changeName', 'btn-outline-warning')
           activate($('#pass'), '.changePass', 'btn-outline-warning')
        }
    })
})
