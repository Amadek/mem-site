$(function() {
    var range = $('#range')
    var meme = $('.meme')
    var string = ""
    meme.click(function() {

        // Meme clicked or not.
        if (!$(this).hasClass('table-warning')) {
            $(this).addClass('table-warning')
            string += $(this).attr('id') + ';'
        } else {
            $(this).removeClass('table-warning')
            string = string.replace($(this).attr('id') + ';', "")
        }

        // Trash script
        var target = '.remMeme'
        if (!string) {
            $('#trash').removeClass('btn-outline-danger')
            $('#trash').addClass('btn-outline-secondary')
            $('#trash').attr('data-target', '')
        } else {
            $('#trash').removeClass('btn-outline-secondary')
            $('#trash').addClass('btn-outline-danger')
            $('#trash').attr('data-target', target)
        }

        range.val(string)
    })
})
