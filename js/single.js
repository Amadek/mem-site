$(function () {
    $('#modal_add_comment').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
    })
    $('#modal_del_comment').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var c_id = button.data('comment')
        modal.find('#comment_id').val(c_id)
        modal.find('#reply_id').val("NULL")
    })
    $('#modal_add_reply').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var c_id = button.data('comment')
        var r_id = button.data('reply')
        modal.find('#comment_id').val(c_id)
        modal.find('#reply_id').val("NULL")
    })
    $('#modal_del_reply').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var c_id = button.data('comment')
        var r_id = button.data('reply')
        modal.find('#comment_id').val(c_id)
        modal.find('#reply_id').val(r_id)
    })
})
