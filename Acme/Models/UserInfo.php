<?php
namespace Acme\Models;

class UserInfo
{
    public $name;
    public $isAdmin;
    public $mCount;

    function __construct($name)
    {
        $this->name = $name;
        $this->isAdmin = $this->isAdmin();
        $this->mCount = $this->mCount();
    }

    private function isAdmin()
    {
        $db = \Flight::get('db');
        $query = 'SELECT isAdmin FROM accounts where nick = :name';
        $stmt = $db->prepare($query);
        $stmt->execute([':name' => $this->name]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $result['isAdmin'];
    }

    protected function mCount()
    {
        $db = \Flight::get('db');
        $query = 'SELECT COUNT(id) as count FROM posts WHERE byWho = :name';
        $stmt = $db->prepare($query);
        $stmt->execute([':name' => $this->name]);
        $table = $stmt->fetch(\PDO::FETCH_ASSOC);

        $mCount = $table['count'];
        return $mCount;
    }
}
