<?php
namespace Acme\Models;

class Reply
{
    public $id;
    public $body;
    public $related;
    public $byWho;
    public $whenAdded;

    function __construct($id)
    {
        $db = \Flight::get('db');
        $query = 'SELECT * FROM replies WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($table) {
            $this->id = $table['id'];
            $this->body = $table['body'];
            $this->related = $table['related'];
            $this->byWho = $table['byWho'];
            $this->whenAdded = $table['whenAdded'];
        } else {
            echo "nie ma takiej odpowiedzi na komentarz.";
        }
    }

    public static function showAll(int $comment_id)
    {
        $db = \Flight::get('db');
        $query = 'SELECT id FROM replies WHERE related = :comment_id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':comment_id', $comment_id, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetchAll();

        $replies = [];
        foreach ($table as $row) {
            $reply = new Reply($row['id']);
            $replies[$reply->id] = $reply;
        }

        return $replies;
    }

    public static function create(string $body, int $comment_id, string $user_name)
    {
        $db = \Flight::get('db');
        $query = 'INSERT INTO replies VALUES (NULL, :body, :related, :byWho, NOW())';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':body', $body);
        $stmt->bindParam(':related', $comment_id, \PDO::PARAM_INT);
        $stmt->bindParam(':byWho', $user_name);
        $stmt->execute();
    }

    public function remove()
    {
        $db = \Flight::get('db');
        $query = 'DELETE FROM replies WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':id' => $this->id
        ]);
    }
}
