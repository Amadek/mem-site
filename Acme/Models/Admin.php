<?php
namespace Acme\Models;
use Acme\Models\User as User;
use Acme\Models\Meme as Meme;
use Acme\Models\Accounts as Accounts;

class Admin extends User
{
    public $users;
    public $allMemes;

    function __construct($name)
    {
        parent::__construct($name);
        $this->users = User::getAll();
        $this->memes = Meme::getAll();
    }

    public function changeUserName($old_name, $new_name)
    {
        User::changeName($old_name, $new_name);
        $this->users = User::getAll();
    }

    public function changeUserPass($name, $new_pass)
    {
        Accounts::changePass($name, $new_pass);
    }

    public function removeUser(string $name)
    {
        $this->users[$name]->remove();
        $this->users = User::getAll();
    }

    public function FilterUsers(string $like)
    {
        $this->users = User::getAll($like);
    }
}
