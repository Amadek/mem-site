<?php
namespace Acme\Models;

class Accounts
{
    const SALT = 'P0l1cj4';

    public static function register($nick, $pass)
    {
        $nick = ($nick == "") ? null : $nick;
        $pass = ($pass == "") ? null : crypt($pass, self::SALT);

        $db = \Flight::get('db');

        // Try insert data.
        $query = 'INSERT INTO accounts VALUES(NULL, :nick, :pass, 0)';
        $stmt = $db->prepare($query);

        try {
            $stmt->execute(array(
                ':nick' => $nick,
                ':pass' => $pass
            ));
            return [
                'type' => "alert-success", // Bootstrap alert style.
                'info' => "Zarejestrowano pomyślnie."
            ];

        } catch (\PDOException $e) {
            return [
                'type' => "alert-danger", // Bootstrap alert style.
                'info' => $e->getMessage()
            ];
        }
    }

    public static function ifExists($nick, $pass)
    {
        $db = \Flight::get('db');
        $pass = crypt($pass, self::SALT);
        $query = 'SELECT * FROM accounts WHERE nick = :nick AND pass = :pass';
        $stmt = $db->prepare($query);
        $stmt->execute(array(
            ':nick' => $nick,
            ':pass' => $pass
        ));

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    }

    public static function changePass($name, $new_pass)
    {
        $new_pass = crypt($new_pass, self::SALT);

        $db = \Flight::get('db');
        $query = 'UPDATE accounts SET pass = :new_pass WHERE nick = :name';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':new_pass' => $new_pass,
            ':name' => $name
        ]);
    }
}
