<?php
namespace Acme\Models;

use Acme\Models\Comment as Comment;

class Meme
{
    public static $count = 0;

    public $id;
    public $title;
    public $whenAdded;
    public $byWho;
    public $whenInDays;
    public $comments;

    function __construct($id)
    {
        $db = \Flight::get('db');
        $query = 'SELECT * FROM posts WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetch(\PDO::FETCH_ASSOC);

        if ($table) {
            $this->id = $id;
            $this->title = $table['title'];
            $this->whenAdded = $table['whenAdded'];
            $this->byWho = $table['byWho'];
            $this->whenInDays = $this->_howManyDays();
            $this->flushComments();
        } else {
            echo "Nie ma takiego mema!";
        }
    }

    public function flushComments()
    {
        $this->comments = Comment::showAll($this->id);
    }

    public function addComment(string $text, string $user_name)
    {
        Comment::create($text, $this->id, $user_name);
        $this->flushComments();
    }

    public function delComment(int $comment_id)
    {
        $this->comments[$comment_id]->remove();
        $this->flushComments();
    }

    // Return date in elapsed days.
    private function _howManyDays()
    {
        $today = date('Y-m-d H:i:s'); // SQL syntax.
        $when = round((strtotime($today) - strtotime($this->whenAdded)) / 86400);

        if ($when < 1) {
            $when = "Niecały dzień temu.";
        } else if($when >= 1 && $when < 2) {
            $when = "1 dzień temu.";
        } else {
            $when = $when . " dni temu.";
        }

        return $when;
    }

    public function remove()
    {
        $db = \Flight::get('db');

        // Delete all related comments and included replies.
        foreach ($this->comments as $comment) {
            $comment->remove();
        }

        $query = 'DELETE FROM posts WHERE id = :id';

        $db->beginTransaction();
        $stmt = $db->prepare($query);

        // Try delete rows, which was clicked.
        try {
            $stmt->bindParam(':id', $this->id, \PDO::PARAM_INT);
            $stmt->execute();

            // Delete related image.
            $file = "memes/" . $this->id . ".jpg";
            if (file_exists($file)) {
                unlink($file);
            } else {
                throw new \Exception("No such a file.");
            }

            $db->commit();
            return [
                'type' => "alert-success",
                'info' => "Usunięto pomyślnie."
            ];

        } catch (\Exception $e) {
            $db->rollBack();
            return [
                'type' => "alert-danger",
                'info' => $e->getMessage()
            ];
        }
    }

    public function changeTitle(string $new_title)
    {
        $db = \Flight::get('db');
        $query = 'UPDATE posts SET title = :title WHERE  id = :id';
        $stmt = $db->prepare($query);
        try
        {
            $stmt->execute([
                ':title' => $new_title,
                ':id' => $this->id
            ]);

            $this->title = $new_title;
        } catch (\Exception $e)
        {
            echo "changeTitle(): Coś poszło nie tak";
        }
    }

    public static function create($title, $img, $owner)
    {
        $title = ($title == "") ? null : $title;
        $db = \Flight::get('db');
        $query = 'INSERT INTO posts VALUES (NULL, :title, NOW(), :owner)';

        $db->beginTransaction();
        $stmt = $db->prepare($query);

        // Try to add new meme title to db and img to dir.
        try {
            $stmt->execute([
                ':title' => $title,
                ':owner' => $owner
            ]);

            // Upload a file.
            $img_params = @getimagesize($img);
            if ($img_params != false && $img_params['mime'] == "image/jpeg") {
                // Add img to current title.
                $img_id = self::lastId();
                move_uploaded_file($img, 'memes/' . $img_id . '.jpg');
            } else {
                throw new \Exception("Obrazek nie jest typu jpg/jpeg.");
            }

            $db->commit();

            return [
                'type' => "alert-success",
                'info' => "Dodano pomyślnie."
            ];

        } catch (\Exception $e) {
            $db->rollBack();
            return [
                'type' => "alert-danger",
                'info' => $e->getMessage()
            ];
        }
    }

    public static function getAll()
    {
        $db = \Flight::get('db');
        $query = 'SELECT * FROM posts';
        $stmt = $db->prepare($query);
        $stmt->execute();
        $table = $stmt->fetchAll();

        $memes = [];
        foreach ($table as $row) {
            $meme = new Meme($row['id']);
            array_push($memes, $meme);
        }

        return $memes;
    }

    public static function pageAll(int $page_nr, int $count)
    {
        $db = \Flight::get('db');
        $offset = ($page_nr - 1) * $count;
        $query = 'SELECT * FROM posts ORDER BY whenAdded DESC LIMIT :count OFFSET :offset';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':count', $count, \PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetchAll();

        $memes = [];
        foreach ($table as $row) {
            $meme = new Meme($row['id']);
            array_push($memes, $meme);
        }

        return $memes;
    }

    public static function searchAll(string $title, int $page_nr, int $count)
    {
        $db = \Flight::get('db');
        $offset = ($page_nr - 1) * $count;
        $query = 'SELECT * FROM posts WHERE title LIKE :title LIMIT :count OFFSET :offset';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':count', $count, \PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetchAll();

        $memes = [];
        foreach ($table as $row) {
            $meme = new Meme($row['id']);
            array_push($memes, $meme);
        }
        if ($memes) {
            return $memes;
        } else {
            return "Brak wyników";
        }
    }

    public static function totalPages(int $onSite)
    {
        $db = \Flight::get('db');
        $query = 'SELECT COUNT(id) AS count FROM posts';
        $stmt = $db->query($query);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        $count = $result['count'];

        $pages = ceil($count / $onSite);

        return $pages;
    }

    public static function findedPages(string $the_title, int $onSite)
    {
        $db = \Flight::get('db');
        $query = 'SELECT COUNT(id) AS count FROM posts WHERE title LIKE :title';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':title', $the_title);
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        $count = $result['count'];

        $pages = ceil($count / $onSite);

        return $pages;
    }

    public static function randomId()
    {
        $db = \Flight::get('db');
        $query = 'SELECT id FROM posts order by RAND() LIMIT 1';
        $stmt = $db->query($query); // array(array('a', 'b'))
        $table = $stmt->fetch(\PDO::FETCH_ASSOC); // array('a', 'b')
        $the_id = $table['id'];
        return $the_id;
    }

    public static function lastId()
    {
        $db = \Flight::get('db');
        $query = 'SELECT MAX(id) AS id FROM posts';
        $stmt = $db->query($query); // array(array('a', 'b'))
        $table = $stmt->fetch(\PDO::FETCH_ASSOC); // array('a', 'b')
        $the_id = $table['id'];
        return $the_id;
    }
}
