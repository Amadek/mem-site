<?php
namespace Acme\Models;

use Acme\Models\UserInfo as UserInfo;
use Acme\Models\Meme as Meme;

class User extends UserInfo
{
    public $memes;

    function __construct($name)
    {
        parent::__construct($name);
        $this->flushMemes();
    }

    function remove()
    {
        foreach ($this->memes as $meme) {
            $meme->remove();
        }

        $db = \Flight::get('db');
        $query = 'DELETE FROM accounts WHERE nick = :nick';
        $stmt = $db->prepare($query);

        $stmt->bindParam(':nick', $this->name);
        $stmt->execute();
    }

    public function flushMemes(string $what = "")
    {
        $db = \Flight::get('db');
        switch ($what) {
            case "title":
                $query = 'SELECT * FROM posts WHERE byWho = :name ORDER BY title';
                break;
            case "whenAdded":
                $query = 'SELECT * FROM posts WHERE byWho = :name ORDER BY whenAdded';
                break;
            default:
                $query = 'SELECT * FROM posts WHERE byWho = :name';
                break;
        }
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':name' => $this->name
        ]);
        $table = $stmt->fetchAll();

        $memes = [];

        foreach ($table as $row) {
            $meme = new Meme($row['id']);
            $memes[(string)$meme->id] = $meme;
        }

        $this->memes = $memes;
        $this->mCount = parent::mCount();
    }

    public function getMemesLike(string $like)
    {
        $db = \Flight::get('db');
        $query = 'SELECT * FROM posts WHERE byWho = :name AND title LIKE :dupa';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':name' => $this->name,
            ':dupa' => $like
        ]);

        $table = $stmt->fetchAll();

        $memes = [];

        foreach ($table as $row) {
            $meme = new Meme($row['id']);
            $memes[(string)$meme->id] = $meme;
        }
        $this->memes = $memes;
    }

    public function addMeme($title, $img)
    {
        $alert = Meme::create($title, $img, $this->name);
        $this->flushMemes();
        // Return success or error.
        return $alert;
    }

    public function remMemes($range)
    {
        $alert = [];
        foreach ($range as $number) {
            $alert = $this->memes[$number]->remove();
            // If something wrong...
            if($alert['type'] == "alert-danger") {
                return $alert;
            }
            unset($this->memes[$number]);
        }
        $this->flushMemes();
        return $alert;
    }

    public static function getAll(string $like = "%%")
    {
        $db = \Flight::get('db');
        $query = 'SELECT nick FROM accounts WHERE nick LIKE :like';
        $stmt = $db->prepare($query);
        $stmt->execute([':like' => $like]);
        $table = $stmt->fetchAll();

        $users = [];
        foreach ($table as $row) {
            $user = new User($row[0]);
            $users[$user->name] = $user;
        }
        return $users;
    }

    public static function changeName($old_name, $new_name)
    {
        $db = \Flight::get('db');
        $query = 'UPDATE accounts SET nick = :new_name WHERE nick = :old_name';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':old_name' => $old_name,
            ':new_name' => $new_name
        ]);
    }
}
