<?php
namespace Acme\Models;

use Acme\Models\Meme as Meme;
use Acme\Models\Reply as Reply;

class Comment
{
    public $id;
    public $body;
    public $related;
    public $byWho;
    public $whenAdded;
    public $replies;

    function __construct(int $id)
    {
        $db = \Flight::get('db');
        $query = 'SELECT * FROM comments WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($table) {
            $this->id = $table['id'];
            $this->body = $table['body'];
            $this->related = $table['related'];
            $this->byWho = $table['byWho'];
            $this->whenAdded = $table['whenAdded'];
            $this->flushReplies();
        } else {
            echo "nie ma takiego komentarza";
        }
    }

    public function flushReplies()
    {
        $this->replies = Reply::showAll($this->id);
    }

    public function addReply(string $body, string $user_name)
    {
        Reply::create($body, $this->id, $user_name);
        $this->flushReplies();
    }

    public function delReply(int $reply_id)
    {
        $this->replies[$reply_id]->remove();
        $this->flushReplies();
    }

    public static function showAll(int $meme_id)
    {
        $db = \Flight::get('db');
        $query = 'SELECT id FROM comments WHERE related = :meme_id';
        $stmt = $db->prepare($query);
        $stmt->bindParam(':meme_id', $meme_id, \PDO::PARAM_INT);
        $stmt->execute();
        $table = $stmt->fetchAll();

        $comments = [];
        foreach ($table as $row) {
            $comment = new Comment($row['id']);
            $comments[$comment->id] = $comment;
        }

        return $comments;
    }

    public static function create(string $text, int $meme_id, string $user_name)
    {
        $db = \Flight::get('db');
        $query = 'INSERT INTO comments VALUES(NULL, :body, :related, :byWho, NOW())';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':body' => $text,
            ':related' => $meme_id,
            ':byWho' => $user_name
        ]);
    }

    public function remove()
    {
        $db = \Flight::get('db');
        // Destroy your replies.
        foreach ($this->replies as $reply) {
            $reply->remove();
        }
        // Destroy yourself.
        $query = 'DELETE FROM comments WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->execute([
            ':id' => $this->id
        ]);
    }
}
