<?php
namespace Acme\Views;

class Views
{
    // For tests.
    public static function test()
    {
        echo \Flight::twig()->render('test.html.twig');
    }

    // Main site, default.
    public static function main($page_nr, $posts, $totalPages, $you)
    {
        echo \Flight::twig()->render('posts.html.twig', array(
            'action' => "main",
            'page_nr' => $page_nr,
            'posts' => $posts,
            'totalPages' => $totalPages,
            'you' => $you
        ));
    }

    // Random or single meme from DB.
    public static function single($meme, $author, $you, $action)
    {
        echo \Flight::twig()->render('single.html.twig',array(
            'meme' => $meme,
            'author' => $author,
            'you' => $you,
            'action' => $action
        ));
    }

    public static function login($alert)
    {
        echo \Flight::twig()->render('login.html.twig', array(
            'alert' => $alert,
            'action' => "login"
        ));
    }

    public static function register($alert)
    {
        echo \Flight::twig()->render('register.html.twig', array(
            'alert' => $alert,
            'action' => "register"
        ));
    }

    public static function profile($you, $alert)
    {
        echo \Flight::twig()->render('profile.html.twig', array(
            'action' => "profile",
            'you' => $you,
            'alert' => $alert
        ));
    }
}
