<?php
namespace Acme\Views;

class AdminViews
{
    public static function main($admin)
    {
        echo \Flight::twig()->render('admin_main.html.twig', [
            'admin' => $admin,
            'action' => "admin"
        ]);
    }
}
