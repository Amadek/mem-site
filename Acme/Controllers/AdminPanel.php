<?php
namespace Acme\Controllers;
use Acme\Views\AdminViews as AdminViews;
use Acme\Models\User as User;
use Acme\Models\Admin as Admin;

class AdminPanel
{
    public static function main()
    {
        if (isset($_SESSION['admin']) && $_SESSION['admin'])
        {
            $admin = new Admin($_SESSION['nick']);
            if (isset($_POST["removeAcc"])) {
                $user_name = $_POST["the_name"];
                $admin->removeUser($user_name);
            }

            if (isset($_POST['changeName'])) {
                $old_name = $_POST['old_name'];
                $new_name = $_POST['new_name'];
                $admin->changeUserName($old_name, $new_name);
            }

            if (isset($_POST['changePass'])) {
                $the_name = $_POST['the_name'];
                $new_pass = $_POST['new_pass'];
                $admin->changeUserPass($the_name, $new_pass);
            }

            if (isset($_POST['find'])) {
                $like = $_POST['search_name'];
                $admin->FilterUsers("%" . $like . "%");
            }

            AdminViews::main($admin);
        } else {
            \Flight::redirect('/');
        }
    }
}
