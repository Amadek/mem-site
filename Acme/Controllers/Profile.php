<?php
namespace Acme\Controllers;

use Acme\Models\User as User;
use Acme\Models\Admin as Admin;
use Acme\Views\Views as Views;

class Profile
{
    static $alert = null;

    public static function profile()
    {
        // if session set from Login::login().
        if (isset($_SESSION['nick'])) {
            if ($_SESSION['admin'])
                $you = new Admin($_SESSION['nick']);
            else
                $you = new User($_SESSION['nick']);
            // Add user defined meme.
            if (isset($_POST['add_image'])) {
                $user_title = $_POST['user_title'];
                $img = $_FILES['file']['tmp_name'];
                self::$alert = $you->addMeme($user_title, $img);
            }

            // Remove user defined meme.
            if (isset($_POST['del_range'])) {
                $range = $_POST['range'];
                $range = substr($range, 0, -1); // "1;2;3;" -> "1;2;3"
                $range = explode(';', $range); // "1;2;3" -> [1, 2, 3]
                self::$alert = $you->remMemes($range);
            }

            // Filter by title.
            if (isset($_POST['filter_title'])) {
                $you->flushMemes("title");

            } else if(isset($_POST['filter_title'])) {
                $you->flushMemes("whenAdded");
            }

            if (isset($_POST['find']))
            {
                $the_title = $_POST['the_title'];
                $you->getMemesLike('%' . $the_title . '%');
            }
            Views::profile($you, self::$alert);

        } else {
            \Flight::redirect('/login');
        }
    }
}
