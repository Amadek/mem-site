<?php
namespace Acme\Controllers;

use Acme\Models\Accounts as Accounts;
use Acme\Views\Views as Views;
use Acme\Models\UserInfo as UserInfo;

class Login
{
    static $alert = null;
    public static function logIn()
    {
        // If logged in.
        if (!isset($_SESSION['nick'])) {
            // Sign in.
            if (isset($_POST['login'])) {

                $nick = $_POST['nick'];
                $pass = $_POST['pass'];

                if (Accounts::ifExists($nick, $pass)) {
                    $_SESSION['nick'] = $nick;
                    $_SESSION['admin'] = false;
                    $user = new UserInfo($nick);

                    if ($user->isAdmin) {
                        $_SESSION['admin'] = true;
                        \Flight::redirect('/admin');
                    } else {
                        \Flight::redirect('/');
                    }

                } else {
                    self::$alert = [
                        'type' => "alert-danger", // Bootstrap alert style.
                        'info' => "Błedna nazwa lub hasło."
                    ];
                }
            }
            Views::login(self::$alert);
        } else {
            \Flight::redirect('/profile');
        }
    }

    public static function logOut()
    {
        session_destroy();
        \Flight::redirect('/');
    }

    public static function register()
    {
        // If logged in.
        if (!isset($_SESSION['nick'])) {
            if (isset($_POST['register'])) {
                $nick = $_POST['nick'];
                $pass = $_POST['pass'];
                $repeat = $_POST['repeat'];
                if ($pass == $repeat)
                    self::$alert = Accounts::register($nick, $pass);
                else
                    self::$alert = [
                        'type' => "alert-danger",
                        'info' => "Hasła nie zgadzają się."
                    ];
            }
            Views::register(self::$alert);
        } else {
            \Flight::redirect('/profile');
        }
    }
}
