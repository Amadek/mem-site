<?php
namespace Acme\Controllers;

use Acme\Models\Accounts as Accounts;
use Acme\Models\User as User;
use Acme\Models\UserInfo as UserInfo;
use Acme\Models\Meme as Meme;
use Acme\Models\Comment as Comment;
use Acme\Models\Reply as Reply;
use Acme\Views\Views as Views;

class Test
{
    public static function test()
    {
        echo "<pre>";
        $meme = new Meme(7);
        $meme->changeTitle("Costam");
        print_r($meme);
        // Views::test();
    }
}
