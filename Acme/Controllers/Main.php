<?php
namespace Acme\Controllers;

use Acme\Models\UserInfo as UserInfo;
use Acme\Models\User as User;
use Acme\Models\Meme as Meme;
use Acme\Models\Comment as Comment;
use Acme\Models\Reply as Reply;
use Acme\Views\Views as Views;

class Main
{
    public static function main(int $page_nr)
    {
        $onSite = 5;
        if ($page_nr <= Meme::totalPages($onSite)) {
            $posts = Meme::pageAll($page_nr, $onSite);

            $you = null;
            if (isset($_SESSION['nick'])) {
                $you = new UserInfo($_SESSION['nick']);
            }

            Views::main($page_nr, $posts, Meme::totalPages($onSite), $you);
        } else {
            echo "Not found";
        }
    }

    public static function find(int $page_nr) {
        $onSite = 3;
        if (isset($_POST['find'])) {
            $the_title = $_POST['the_title'];
            $_SESSION['the_title'] = $the_title;
        }

        if (isset($_SESSION['the_title'])) {
            $the_title = '%'. $_SESSION['the_title'] . '%';
            $posts = Meme::searchAll($the_title, $page_nr, $onSite);
            $you = null;
            if (isset($_SESSION['nick'])) {
                $you = new UserInfo($_SESSION['nick']);
            }
            Views::main($page_nr, $posts, Meme::findedPages($the_title, $onSite), $you);
        } else {
            \Flight::redirect('/');
        }
    }

    public static function single($single, $action = "single")
    {
        $meme = new Meme($single);
        $author = new UserInfo($meme->byWho);

        $you = null;
        if (isset($_SESSION['nick'])) {
            $nick = $_SESSION['nick'];
            $you = new UserInfo($nick);

            if (isset($_POST['add_comment'])) {
                $text = $_POST['comment'];
                $meme->addComment($text, $you->name);
            }

            if (isset($_POST['del_comment'])) {
                $id = $_POST['comment_id'];
                $meme->delComment($id);
            }

            if (isset($_POST['add_reply'])) {
                $text = $_POST['comment'];
                $c_id = $_POST['comment_id'];
                $meme->comments[$c_id]->addReply($text, $you->name);
            }

            if (isset($_POST['del_reply'])) {
                $c_id = $_POST['comment_id'];
                $r_id = $_POST['reply_id'];
                $meme->comments[$c_id]->delReply($r_id, $you->name);
            }
        }

        Views::single($meme, $author, $you, $action);
    }

    public static function random()
    {
        $random_id = Meme::randomId();
        // Do the same thing what in single(), but with another action value.
        self::single($random_id, $action = "random");
    }
}
