<?php
require 'vendor/autoload.php';

// DB try connect.
Flight::register('db', 'PDO', array('mysql:host=localhost;dbname=mem_site', 'root', ''));
try {
    $db = \Flight::db();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // Return DB elsewhere.
    Flight::set('db', $db);
} catch (PDOException $e) {
    die($e->getMessage());
}

// Twig templates \Fligt::twig()
Flight::map('twig', function() {
    $loader = new \Twig_Loader_Filesystem('Acme/Templates');
    $twig = new \Twig_Environment($loader);
    return $twig;
});

session_start();

// Main route, default.
Flight::route('/', function() {
    Flight::redirect('/1');
});

// Seek your memes.
Flight::route('/find/@page_nr:[0-9]+', function(int $page_nr) {
    Acme\Controllers\Main::find($page_nr);
});

// Route with paginatotion.
Flight::route('/@page_nr:[0-9]+', function(int $page_nr) {
    Acme\Controllers\Main::main($page_nr);
});

// Single meme.
Flight::route('/single/@single:[0-9]+', function(int $single) {
    Acme\Controllers\Main::single($single);
});

// Random meme.
Flight::route('/losowe', array('Acme\Controllers\Main', 'random'));

// Accounts
Flight::route('/login', array('Acme\Controllers\Login', 'logIn'));
Flight::route('/register', array('Acme\Controllers\Login', 'register'));
Flight::route('/log_out', array('Acme\Controllers\Login', 'logOut'));
Flight::route('/profile', array('Acme\Controllers\Profile', 'profile'));

// For tests.
Flight::route('/test', array('Acme\Controllers\Test', 'test'));

// Admin site
Flight::route('/admin', array('Acme\Controllers\AdminPanel', 'main'));

Flight::start();
